import com.github.aakira.napier.Antilog
import com.github.aakira.napier.LogLevel

class ConsoleAntilog : Antilog() {
    override fun performLog(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        println("[${priority.name}] Tag=$tag: $message")
        throwable?.let {
            println(it.message)
            it.printStackTrace()
        }
    }
}