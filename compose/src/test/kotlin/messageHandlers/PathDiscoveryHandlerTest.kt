package messageHandlers

import ConsoleAntilog
import com.github.aakira.napier.Napier
//import kotlinx.serialization.json.Json
//import kotlinx.serialization.modules.SerializersModule
import me.kacper.barszczewski.common.*
import me.kacper.barszczewski.common.utils.LoggerWrapper
import me.kacper.barszczewski.entities.InitializationInformationEntity
import me.kacper.barszczewski.entities.PathDiscoveryEntity
import me.kacper.barszczewski.entities.SerializationModule
import me.kacper.barszczewski.entities.request.RequestInitializationInformation
import me.kacper.barszczewski.entities.request.RequestPathDiscovery
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import java.io.File
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.test.assertTrue

class PathDiscoveryHandlerTest {

    private val logger = LoggerWrapper()

    data class Locker(
        val lock: ReentrantLock = ReentrantLock(),
        val condition: Condition = lock.newCondition(),
        var signalQueue: Int = 0
    ) {

        fun signalAfter(run: Runnable) {
            lock.withLock {
                run.run()
                signalQueue++
                condition.signalAll()
            }
        }

        fun await() {
            lock.withLock {
                if (signalQueue == 0) {
                    condition.await()
                }
                signalQueue--
            }
        }

        fun signal() {
            lock.withLock {
                signalQueue++
                condition.signalAll()
            }
        }

    }

    companion object {
        @BeforeClass
        @JvmStatic
        fun beforeTest() {
            // Register modules
            SerializationModule.register()

            Napier.base(ConsoleAntilog())
            println("#".repeat(50))
        }

        @AfterClass
        @JvmStatic
        fun afterTest() {
            println("#".repeat(50))
        }

        private val locks: MutableMap<Long, Locker> = mutableMapOf()

        private fun getLock(ID: Long): Locker = locks.getOrPut(ID) { Locker() }
        fun signalAfter(id: Long, run: Runnable) = getLock(id).signalAfter(run)
        fun await(id: Long) = getLock(id).await()
        fun signal(id: Long) = getLock(id).signal()

    }


    @Test(timeout = 2000)
    fun initializationMessageTest1() {

        val ID = 1L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        InitializationInformationHandler.register(remoteConnection)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        remoteClient.addMessageHandler(InitializationInformationEntity.CODE) {
            signal(ID)
        }

        remoteClient.sendMessage(RequestInitializationInformation(false))
        await(ID)
    }

    @Test
    fun pathDiscoveryTest1() {
        val ID = 2L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        PathDiscoveryHandler.register(remoteConnection)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        var baseFile = File("testDirectory")

        remoteClient.addMessageHandler(PathDiscoveryEntity.CODE) {
            val path1 = it.deserialize<PathDiscoveryEntity>()
            Assert.assertEquals(1, path1.directories.size)
            Assert.assertEquals(1, path1.files.size)
            Assert.assertEquals("File1.txt", path1.files[0])
            Assert.assertEquals("parent1", path1.directories[0])
            logger.debug { "First level checked" }
            signal(ID)
        }
        remoteClient.sendMessage(RequestPathDiscovery(baseFile.absolutePath))

        await(ID)
        remoteClient.messageHandler[PathDiscoveryEntity.CODE]?.clear()
        remoteClient.addMessageHandler(PathDiscoveryEntity.CODE) {
            val path1 = it.deserialize<PathDiscoveryEntity>()
            Assert.assertEquals(1, path1.directories.size)
            Assert.assertEquals(1, path1.files.size)
            Assert.assertEquals("File2.txt", path1.files[0])
            Assert.assertEquals("parent2", path1.directories[0])
            logger.debug { "Seconde level checked" }
            signal(ID)
        }
        baseFile = File(baseFile, "parent1")
        remoteClient.sendMessage(RequestPathDiscovery(baseFile.absolutePath))

        await(ID)
        remoteClient.messageHandler[PathDiscoveryEntity.CODE]?.clear()
        remoteClient.addMessageHandler(PathDiscoveryEntity.CODE) {
            val path1 = it.deserialize<PathDiscoveryEntity>()
            Assert.assertEquals(0, path1.directories.size)
            Assert.assertEquals(2, path1.files.size)
            val sortedFiles = path1.files.sortedArray()
            Assert.assertEquals("File3.txt", sortedFiles[0])
            Assert.assertEquals("File4.txt", sortedFiles[1])
            logger.debug { "Third level checked" }
            signal(ID)
        }
        baseFile = File(baseFile, "parent2")
        remoteClient.sendMessage(RequestPathDiscovery(baseFile.absolutePath))
        await(ID)
    }

    private fun connectToServerAndReturn(
        PORT: Long,
        ID: Long
    ): RemoteClient {
        var remoteClient: RemoteClient? = null
        RemoteConnection.connectToServer("localhost", PORT.toInt(), callback = {
            signalAfter(ID) {
//                println("Connected to server localhost:$PORT")
                assertTrue { !it.closed }
                remoteClient = it
            }
        }, startThread = true)
        await(ID)
        return remoteClient ?: throw RuntimeException("RemoteConnection is null")
    }

    @Suppress("DeferredResultUnused")
    private fun createServerConnectionAndReturn(
        PORT: Long,
        ID: Long
    ): RemoteServerExt {
        var remoteServer: RemoteServerExt? = null

        RemoteConnection.createServer(
            PORT.toInt(),
            creator = { RemoteServerExt(it) },
            callback = {
                signalAfter(ID) {
//                println("Server started, signaling lock...")
                    assertTrue { !it.serverSocket.isClosed }
                    remoteServer = it
                }
            }
        )

        await(ID)
        return remoteServer ?: throw RuntimeException("RemoteServer is null")
    }

}