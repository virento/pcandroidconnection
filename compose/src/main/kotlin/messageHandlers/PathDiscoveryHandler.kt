package messageHandlers

import me.kacper.barszczewski.common.MessageData
import me.kacper.barszczewski.common.sendMessage
import me.kacper.barszczewski.common.utils.LoggerWrapper
import me.kacper.barszczewski.entities.PathDiscoveryEntity
import me.kacper.barszczewski.entities.request.RequestPathDiscovery
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.DosFileAttributes
import java.util.function.Function
import java.util.stream.Collectors
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.isReadable
import kotlin.io.path.isSymbolicLink
import kotlin.io.path.readAttributes

class PathDiscoveryHandler(
    val remoteServer: RemoteServerExt
) {

    private val logger = LoggerWrapper()

    companion object {
        fun register(remoteServer: RemoteServerExt) {
            val pathDiscoveryHandler = PathDiscoveryHandler(remoteServer)
            remoteServer.addMessageHandler(RequestPathDiscovery.CODE) { pathDiscoveryHandler.handlerMessage(it) }
        }
    }

    @OptIn(ExperimentalPathApi::class)
    protected fun handlerMessage(messageData: MessageData) {
        val entity = messageData.deserialize<RequestPathDiscovery>()
        val path = Path.of(entity.path)

        if (!Files.exists(path)) {
            logger.info { "Path '$path' doesn't exists" }
            return //TODO: Throw exception
        }

        val function: Function<Path, Boolean> = Function { t -> Files.isDirectory(t) }

        var filesList = Files.list(Path.of(entity.path))
        filesList = filesList.filter { it.isReadable() && !it.isSymbolicLink() }
        if (!entity.includeHidden) {
            filesList = filesList.filter { !it.readAttributes<DosFileAttributes>().isHidden }
        }
        if (!entity.includeSystem) {
            filesList = filesList.filter { !it.readAttributes<DosFileAttributes>().isSystem }
        }

        val collect = filesList.collect(Collectors.groupingBy(function))

        val pathDiscovery = PathDiscoveryEntity(entity.path, wrap(entity, collect[true]), wrap(entity, collect[false]))

        logger.info { "Sending entity: $pathDiscovery" }
        messageData.client.sendMessage(pathDiscovery)
    }

    private fun wrap(request: RequestPathDiscovery, pathsList: List<Path>?): Array<String> {
        if (pathsList == null) {
            return emptyArray()
        }
        var stream = pathsList.stream().map { it.fileName.toString() }.sorted(naturalOrder());
//        if (!request.includeExtension) {
//            stream = stream.map { removeExtensionFromName(it) }
//        }
        return stream.collect(Collectors.toList()).toTypedArray()
    }

    private fun removeExtensionFromName(name: String): String {
        val dotExtension = name.lastIndexOf(".")
        if (dotExtension <= 0) {
            return name
        }
        return name.substring(0, dotExtension)
    }

}