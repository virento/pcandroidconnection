package messageHandlers

import me.kacper.barszczewski.common.ClientServerListenerType
import me.kacper.barszczewski.common.MessageData
import me.kacper.barszczewski.common.RemoteClient
import me.kacper.barszczewski.common.RemoteServer
import java.net.ServerSocket

open class RemoteServerExt(serverSocket: ServerSocket) : RemoteServer(serverSocket) {

    init {
        addClientListener(ClientServerListenerType.NEW) { client ->
            messageHandler.forEach { messageHandlers ->
                messageHandlers.value.forEach {
                    client.addMessageHandler(messageHandlers.key, it)
                }
            }
        }
    }

    var messageHandler: MutableMap<String, MutableList<(MessageData) -> Unit>> = mutableMapOf()

    open fun addMessageHandler(message: String, handler: (MessageData) -> Unit) {
        messageHandler.getOrPut(message) { mutableListOf() }.add(handler)
        clients.forEach { it.addMessageHandler(message, handler) }
    }

    open fun addClientListener(type: ClientServerListenerType, listener: (RemoteClient) -> Unit) {
        clientListeners.getOrPut(type) { mutableListOf() }.add(listener)
    }

}