package messageHandlers

import me.kacper.barszczewski.common.MessageData
import me.kacper.barszczewski.common.sendMessage
import me.kacper.barszczewski.common.utils.LoggerWrapper
import me.kacper.barszczewski.entities.DiscInfo
import me.kacper.barszczewski.entities.InitializationInformationEntity
import me.kacper.barszczewski.entities.request.RequestInitializationInformation
import java.io.File
import javax.swing.filechooser.FileSystemView

class InitializationInformationHandler(
    val remoteServer: RemoteServerExt
) {

    private val logger = LoggerWrapper()

    companion object {
        fun register(remoteServer: RemoteServerExt) {
            val initializationInformationHandler = InitializationInformationHandler(remoteServer)
            remoteServer.addMessageHandler(RequestInitializationInformation.CODE) {
                initializationInformationHandler.handlerMessage(
                    it
                )
            }
        }

    }

    private fun handlerMessage(messageData: MessageData) {
        val partitions = File.listRoots()

        val fsv = FileSystemView.getFileSystemView()
        val homeDirectory = fsv.homeDirectory

        val discs = mutableListOf<DiscInfo>()
        partitions.forEach {
            discs.add(
                DiscInfo(
                    fsv.getSystemDisplayName(it),
                    it.absolutePath,
                    it.absolutePath,
                    it.canWrite(),
                    it.canRead()
                )
            )
        }
        val homeDiscInfo = DiscInfo(
            "Home",
            homeDirectory.absolutePath,
            homeDirectory.absolutePath,
            homeDirectory.canWrite(),
            homeDirectory.canRead()
        )
        val initializationInformationEntity = InitializationInformationEntity(discs, homeDiscInfo)

        messageData.client.sendMessage(initializationInformationEntity)
    }
}