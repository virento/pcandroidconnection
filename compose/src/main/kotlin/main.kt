import androidx.compose.desktop.Window
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.github.aakira.napier.DebugAntilog
import com.github.aakira.napier.Napier
import me.kacper.barszczewski.common.RemoteConnection
import me.kacper.barszczewski.entities.SerializationModule
import messageHandlers.InitializationInformationHandler
import messageHandlers.PathDiscoveryHandler
import messageHandlers.RemoteServerExt

val port = 55556

fun main() = Window {
    SerializationModule.register()

    var text by remember { mutableStateOf("Hello, World!") }

    Napier.base(DebugAntilog())
    Napier.i("Loggin test")

    RemoteConnection.createServer(port, creator = { RemoteServerExt(it) }, callback = {
        InitializationInformationHandler.register(it)
        PathDiscoveryHandler.register(it)
    })

    MaterialTheme {
        Button(onClick = {
            text = "Hello, Desktop!"
        }) {
            Text(text)
        }
    }
}