import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.30"
    id("org.jetbrains.compose") version "0.3.1"
}

group = "me.kacper.barszczewski"
version = "1.0"

repositories {
    jcenter()
    mavenLocal()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
}

val coroutinesVersion = "1.4.3"
val serializationVersion = "1.1.0"
dependencies {
    implementation(project(":PCAndroidConnectionEntities"))
    implementation(compose.desktop.currentOs)

    implementation("me.kacper.barszczewski:TCPConnectionLibrary-jvm:1.3.3-SNAPSHOT")

    implementation("com.github.aakira:napier:1.5.0-alpha1")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serializationVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.20.0")

//    Test library
    implementation(kotlin("test-common"))
    implementation(kotlin("test-annotations-common"))
    implementation(kotlin("test-junit"))
    implementation(kotlin("test"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutinesVersion")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "compose"
            packageVersion = "1.0.0"
        }
    }
}