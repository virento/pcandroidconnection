package me.kacper.barszczewski.entities

import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass
import me.kacper.barszczewski.common.MessageFactory
import me.kacper.barszczewski.common.entities.MessageEntity
import me.kacper.barszczewski.entities.InitializationInformationEntity
import me.kacper.barszczewski.entities.PathDiscoveryEntity
import me.kacper.barszczewski.entities.request.RequestInitializationInformation
import me.kacper.barszczewski.entities.request.RequestPathDiscovery

object SerializationModule {

    fun register() {

        val module = SerializersModule {
            polymorphic(MessageEntity::class) {
                subclass(RequestInitializationInformation::class)
                subclass(InitializationInformationEntity::class)
                subclass(RequestPathDiscovery::class)
                subclass(PathDiscoveryEntity::class)
            }
        }
        MessageFactory.jsonObject = Json { ignoreUnknownKeys = true; serializersModule = module }
    }

}