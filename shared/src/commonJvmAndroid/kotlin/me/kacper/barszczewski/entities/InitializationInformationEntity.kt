package me.kacper.barszczewski.entities

import kotlinx.serialization.Serializable
import me.kacper.barszczewski.common.entities.MessageEntity

@Serializable
data class InitializationInformationEntity(
    val discs: List<DiscInfo>,
    val homeDirectory: DiscInfo
) : MessageEntity() {

    companion object {
        const val CODE = "INITIALIZE_INFO"
    }

    override fun code(): String = CODE
}

@Serializable
data class DiscInfo(
    val name: String,
    val path: String,
    val defaultPath: String,
    val writable: Boolean,
    val readable: Boolean
)