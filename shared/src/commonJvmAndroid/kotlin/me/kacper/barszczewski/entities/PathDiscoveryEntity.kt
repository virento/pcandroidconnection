package me.kacper.barszczewski.entities

import kotlinx.serialization.Serializable
import me.kacper.barszczewski.common.entities.MessageEntity

@Serializable
data class PathDiscoveryEntity(
    val path: String,
    val directories: Array<String>,
    val files: Array<String>
) : MessageEntity() {

    companion object {
        const val CODE = "PATH_DISCOVERY"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PathDiscoveryEntity

        if (path != other.path) return false
        if (!directories.contentEquals(other.directories)) return false
        if (!files.contentEquals(other.files)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = path.hashCode()
        result = 31 * result + directories.contentHashCode()
        result = 31 * result + files.contentHashCode()
        return result
    }

    override fun code(): String {
        return CODE
    }
}