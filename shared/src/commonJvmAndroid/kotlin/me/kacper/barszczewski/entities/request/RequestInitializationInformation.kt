package me.kacper.barszczewski.entities.request

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import me.kacper.barszczewski.common.entities.MessageEntity

@Serializable
data class RequestInitializationInformation(val dump: Boolean) : MessageEntity() {

    companion object {
        const val CODE = "R_INITIALIZE_INFO"
    }

    override fun code(): String = CODE

}