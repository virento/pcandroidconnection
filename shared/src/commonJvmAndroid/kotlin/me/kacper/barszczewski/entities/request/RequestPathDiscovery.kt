package me.kacper.barszczewski.entities.request

import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import me.kacper.barszczewski.common.entities.MessageEntity

@Serializable
class RequestPathDiscovery(
    var path: String,
    var includeHidden: Boolean = false,
    var includeSystem: Boolean = false,
    var includeExtension: Boolean = false
) : MessageEntity() {

    companion object {
        const val CODE = "R_PATH_DISCOVERY"
    }

    override fun code(): String = CODE

}