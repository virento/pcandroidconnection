plugins {
    id("com.android.library")
    kotlin("multiplatform") version "1.4.32"
    kotlin("plugin.serialization") version "1.4.32"
    id("maven-publish")
}

group = "me.kacper.barszczewski"
version = "1.0.3-SNAPSHOT"

repositories {
    google()
    mavenLocal()
    jcenter()
}

val serializationVersion = "1.1.0"

kotlin {
    android()
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serializationVersion")
//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.20.0")

                implementation("me.kacper.barszczewski:TCPConnectionLibrary:1.3.3-SNAPSHOT")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
//        val commonJvmAndroid = create("commonJvmAndroid") {
//            dependsOn(commonMain)
//            dependencies {
//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serializationVersion")
//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.20.0")
//
//                implementation("me.kacper.barszczewski:TCPConnectionLibrary:1.3.1-SNAPSHOT")
//            }
//        }
//        val commonJvmAndroidTest = create("commonJvmAndroidTest") {
//            dependsOn(commonTest)
//            dependencies {
//                implementation(kotlin("test-common"))
//                implementation(kotlin("test-annotations-common"))
//            }
//        }
        val androidMain by getting {
//            dependsOn(commonJvmAndroid)
            kotlin.srcDir("src/commonJvmAndroid/kotlin")
            dependencies {
                implementation("com.google.android.material:material:1.2.1")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13")
            }
        }
        val jvmMain by getting {
//            dependsOn(commonJvmAndroid)
            kotlin.srcDir("src/commonJvmAndroid/kotlin")
        }
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
            }
        }
    }
    android {
        publishLibraryVariantsGroupedByFlavor = true
        publishLibraryVariants("release", "debug")
    }
}

android {
    compileSdkVersion(29)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
//        applicationId = "me.kacper.barszczewski.shared"
        minSdkVersion(22)
        targetSdkVersion(29)
    }
    buildFeatures {
        viewBinding = true
    }
}