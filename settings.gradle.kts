pluginManagement {
    repositories {
        google()
        jcenter()
        gradlePluginPortal()
        mavenCentral()
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.namespace == "com.android" || requested.id.name == "kotlin-android-extensions") {
                useModule("com.android.tools.build:gradle:4.0.1")
            }
        }
    }
    plugins {
        id("maven-publish")
    }
}
rootProject.name = "PCAndroidConnection"


include(":compose")
include(":shared")

project(":shared").name = "PCAndroidConnectionEntities"
